name := "EloSpider"

version := "0.1"

scalaVersion := "2.13.7"

scalacOptions ++= Seq(
  "-language:higherKinds"
)

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.32"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.7.0"
libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3-393-da7c7c7"
libraryDependencies += "org.typelevel" %% "log4cats-slf4j" % "2.1.1"
libraryDependencies += "org.jsoup" % "jsoup" % "1.14.3"
libraryDependencies += "args4j" % "args4j" % "2.33"

libraryDependencies += "org.typelevel" %% "discipline-scalatest" % "2.1.5" % Test
libraryDependencies += "org.typelevel" %% "cats-effect-testing-scalatest" % "1.4.0" % Test
