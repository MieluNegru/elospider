# EloSpider

Simple web crawler using cats-effect.

Can crawl a website up to a given depth, saving its web pages to the local file system.

## Usage

```bash
sbt> run --repositoryDirPath [path-to-dir-where-to-save-webpages] --crawlerStartUrl [init-web-page-url] --crawlerDepth [N]
```

## Code structure

### Algebra
* `crawler.algebra.browser.DocBrowser` - defines primitives for crawling on websites
* `crawler.algebra.repository.DocRepository` - defines primitives for saving down websites to some storage 

### Crawler

`crawler.Crawler` does the actual web crawling, based on a given algebra for browsing and saving (see above)

```scala
class Crawler[F[_] : Temporal : Parallel : Async, Url, Doc](
    browser: DocBrowser[F, Url, Doc],
    repository: DocRepository[F, Url, Doc],
) {
```

The entry point to web crawling is `crawler.Crawler.startCrawl`

## Testing

The algebra abstracts over the URL and HTML Document types, which allows for easy mocking and testing.

```scala
trait DocBrowser[F[_], UrlT, DocT]
```

The test setup provides a custom algebra that mocks web browsing and disk saving operations
`crawler.TestAlgebra`

Running the crawler using this test algebra collects all the crawled mock-websites into `TestAlgebra.result`, 
which can then be used to make assertions in unit tests

```scala
program.asserting { _ =>
  assert(TestAlgebra.result.get.size == TestAlgebra.result.get.distinct.size)
}
```    

## Limitations

* Implementation currently lacks any awareness of *robots.txt*
* Current revision depends on blocking IO libraries -- *JSoup & java.io.File*. Future iterations should replace these with non-blocking.

## Further development

* The crawler's rate limiting is currently hardcoded inside `crawler.Crawler`  
```scala
val rateLimitSemaphore: F[Semaphore[F]] = Semaphore[F](50)
val rateLimitDuration: FiniteDuration = 1.second
```  
Future development should configure the exact parameters using the website's `robots.txt`

* Introduce database layer for saving down website
* Move completely to non-blocking IO libraries
* Testing: wide scope for integration tests on actual HTML documents