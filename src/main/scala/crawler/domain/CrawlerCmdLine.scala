package crawler.domain

import cats.effect.IO
import org.kohsuke.args4j.{Option => Args4JOpt, _}
import org.typelevel.log4cats.slf4j.Slf4jLogger

// not exactly idiomatic Scala with all those vars, Args4J is the library I've used the most
class CrawlerCmdLine {

  @Args4JOpt(
    name = "--repositoryDirPath",
    required = true,
    usage = "Absolute path to directory where to save the crawled web pages"
  )
  var repositoryDirPath: String = _

  @Args4JOpt(
    name = "--crawlerStartUrl",
    required = true,
    usage = "Initial URL where the web crawler will begin"
  )
  var crawlerStartUrl: String = _

  @Args4JOpt(
    name = "--crawlerDepth",
    required = true,
    usage = "Number of links to traverse recursively, after which crawling stops"
  )
  var crawlerDepth: Int = _

}

object CrawlerCmdLine {

  def fromCmdLine(args: Seq[String]): IO[CrawlerCmdLine] =
    for {
      log     <- Slf4jLogger.create[IO]
      cmdLine <- IO(new CrawlerCmdLine)
      parser   = new CmdLineParser(cmdLine)
      _       <- IO(parser.parseArgument(args: _*))
                   .handleErrorWith {
                     case ex: CmdLineException =>
                       log.error(ex.getMessage) >> IO(parser.printUsage(Console.err)) >> IO(sys.exit(-1))
                   }
    } yield cmdLine


}