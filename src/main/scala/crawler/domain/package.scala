package crawler

import scala.concurrent.duration.FiniteDuration

package object domain {

  sealed trait ShouldVisit

  object ShouldVisit {
    final case class Yes(afterTime: FiniteDuration) extends ShouldVisit
    case object No extends ShouldVisit
  }

  final case class RobotsTxt(reqDelay: FiniteDuration, allow: Seq[String], disallow: Seq[String])

  final case class EnqueuedUrl[T](href: T, depth: Int)

}
