package crawler.algebra.browser

import java.net.{URI, URL}

import cats.ApplicativeError
import cats.effect.{Async, Concurrent}
import cats.syntax.all._
import crawler.domain._
import org.jsoup.nodes.Document
import org.jsoup.{Connection, Jsoup}
import org.typelevel.log4cats.Logger

import scala.concurrent.duration._
import scala.jdk.CollectionConverters._
import scala.util.Try

class JSoupDocBrowser[F[_]](baseUrl: String, maxDepth: Int)(
  implicit F: Async[F], C: Concurrent[F], AE: ApplicativeError[F, Throwable], L: Logger[F]
) extends DocBrowser[F, String, Document] {

  private val host = new URI(baseUrl).getHost

  override def robots(url: String): F[RobotsTxt] =
    F.delay(RobotsTxt(1.second, Seq.empty, Seq.empty)) // TODO robots.txt loading & parsing

  override def shouldVisit(url: EnqueuedUrl[String]): F[ShouldVisit] =
    (
      F.delay(url.depth <= maxDepth),
      isValidUrl(url.href),
      isSameHost(url.href),
      robots(url.href).flatMap(r => disallows(url.href, r)).map(!_)
    )
      .forallM[F](identity)
      .ifM(
        ifTrue = for {
          response <- F.delay(Jsoup.connect(url.href).method(Connection.Method.HEAD).execute())
        } yield {
          if (JSoupDocBrowser.allowedContentTypes.exists(response.contentType() contains _)) ShouldVisit.Yes(0.second)
          else ShouldVisit.No
        },
        ifFalse = F.pure(ShouldVisit.No)
      )

  override def loadURL(url: String): F[Document] =
    Logger[F].info(s"Loading URL $url") >>
      F.delay(Jsoup.connect(url).get())

  override def shouldIndex(doc: Document): F[Boolean] =
    F.delay {
      val siteSaysNoIndex = doc.select("meta[name=\"robots\"]")
        .iterator().asScala
        .exists(_.attr("content") == "noindex")
      !siteSaysNoIndex
    }

  override def hrefs(docUrl: String, doc: Document): F[Seq[String]] =
    for {
      links <- F.delay(doc.select("a[href]").iterator().asScala)
      urls  <- F.delay(links.map(_.attr("href")).toSeq)
      absUrls <- F.delay {
                   Try(new URL(docUrl))
                     .toOption
                     .map { docUrlBase =>
                       urls.flatMap(u => Try(new URL(docUrlBase, u)).toOption)
                     }
                     .getOrElse(Seq.empty)
                 }
    } yield absUrls.map(_.toExternalForm)

  def isSameHost(url: String): F[Boolean] =
    F.delay(new URI(url).getHost == host) // really not ideal, but good enough for an exercise
      .recover(_ => false) // malformed url, don't care about it

  def disallows(url: String, robots: RobotsTxt): F[Boolean] =
    F.pure(false) // TODO should include robots info

  def isValidUrl(url: String): F[Boolean] =
    F.delay(new URL(url)).attempt.map(_.isRight)

}

object JSoupDocBrowser {
  val allowedContentTypes: Set[String] = Set("text/html")
}