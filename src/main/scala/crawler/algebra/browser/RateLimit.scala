package crawler.algebra.browser

import cats.effect.std.Semaphore
import cats.effect.{Clock, Temporal}
import cats.effect.syntax.all._
import cats.syntax.all._

import scala.concurrent.duration.FiniteDuration

object RateLimit {

  /*
  Throttling a finite number of requests over a given timespan.
  Pretty much lifted from https://medium.com/disney-streaming/a-rate-limiter-in-15-lines-of-code-with-cats-effect-af09d838857a
  */
  def apply[F[_], A, B](
    f: A => F[B],
    semaphore: Semaphore[F],
    sleepAmt: FiniteDuration
  )(implicit F: Temporal[F], T: Clock[F]): A => F[B] = { (a: A) =>
    for {
      _      <- semaphore.acquire
      timer  <- F.sleep(sleepAmt).start
      result <- f(a)
      _      <- timer.join
      _      <- semaphore.release
    } yield result
  }

}
