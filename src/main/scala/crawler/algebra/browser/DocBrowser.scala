package crawler.algebra.browser

import crawler.domain._

trait DocBrowser[F[_], UrlT, DocT] {
  def robots(url: UrlT): F[RobotsTxt] // well-behaved crawler
  def shouldVisit(url: EnqueuedUrl[UrlT]): F[ShouldVisit]
  def loadURL(url: UrlT): F[DocT]
  def shouldIndex(doc: DocT): F[Boolean]
  def hrefs(docUrl: UrlT, doc: DocT): F[Seq[UrlT]]
}
