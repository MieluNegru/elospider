package crawler.algebra.repository

import java.io.{File, FileWriter, PrintWriter}
import java.net.{URL, URLEncoder}
import java.nio.charset.Charset

import cats.ApplicativeError
import cats.effect.kernel.{Resource, Sync}
import cats.syntax.all._
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.typelevel.log4cats.Logger

import scala.util.Try

class FileSystemDocRepository[F[_]](repositoryDir: File)(
  implicit F: Sync[F], AE: ApplicativeError[F, Throwable], L: Logger[F]
) extends DocRepository[F, String, Document] {

  override def save(url: String, doc: Document): F[Unit] =
    for {
      file <- F.defer(urlToFile(url))
      _    <- createFile(file, url) >>
                Logger[F].info(s"Saving web page $url to file $file") >>
                saveToFile(doc, file)
    } yield ()
  
  override def load(url: String): F[Option[Document]] =
    for {
      file <- F.defer(urlToFile(url))
      doc  <- if (file.isFile) F.delay(Option(Jsoup.parse(file, "UTF-8")))
              else F.pure(None: Option[Document])
    } yield doc
  
  override def isSaved(url: String): F[Boolean] =
    F.defer(urlToFile(url))
      .flatMap(f => F.delay(f.isFile))
      .handleError(_ => false)

  private def createFile(file: File, fromUrl: String): F[Unit] = F.defer {
    if (file.exists()) Logger[F].info(s"Url $fromUrl was already scraped to ${file.getAbsolutePath}")
    else {
      val parent = file.getParentFile
      val makeParent =
        if (parent.isDirectory) F.unit
        else F.delay(parent.mkdirs()).void
      makeParent >>
        F.delay(file.createNewFile())
          .attempt
          .map {
            case Left(ex) => throw new IllegalStateException(s"Error creating file $file for url $fromUrl", ex)
            case Right(_) => ()
          }
          .void
    }
  }

  private def saveToFile(doc: Document, file: File): F[Unit] =
    for {
      html <- F.delay(doc.outerHtml())
      res   = Resource.make[F, PrintWriter](F.delay(new PrintWriter(new FileWriter(file))))(w => F.delay(w.close()))
      _    <- res.use { w => F.delay(w.write(html)) }
    } yield ()

  private def urlToFile(crawlUrl: String): F[File] =
    F.delay {
      val url = new URL(crawlUrl)
      // 'normalising' the URL like this is not ideal, but it gets rid of unwanted characters on a system like Windows.
      val host = URLEncoder.encode(url.getHost, charset)
      val path = URLEncoder.encode(url.getPath, charset)

      val thePath = if (path.isEmpty) "index.html" else path
      new File(repositoryDir, s"$host/$thePath")
    }

  private val charset = Try(Charset.forName("UTF-8")).getOrElse(Charset.defaultCharset())

}
