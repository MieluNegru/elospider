package crawler.algebra.repository

trait DocRepository[F[_], UrlT, DocT] {
  def save(url: UrlT, doc: DocT): F[Unit]
  def load(url: UrlT): F[Option[DocT]]
  def isSaved(url: UrlT): F[Boolean]
}
