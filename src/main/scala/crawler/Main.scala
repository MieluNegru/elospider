package crawler

import java.io.File

import cats.effect._
import crawler.algebra.browser.JSoupDocBrowser
import crawler.algebra.repository.FileSystemDocRepository
import crawler.domain.CrawlerCmdLine
import org.jsoup.nodes.Document
import org.typelevel.log4cats.slf4j.Slf4jLogger

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    Slf4jLogger.create[IO].flatMap { implicit logger =>
      for {
        cmdLine  <- CrawlerCmdLine.fromCmdLine(args)
        crawler   = new Crawler[IO, String, Document](
          new JSoupDocBrowser[IO](cmdLine.crawlerStartUrl, cmdLine.crawlerDepth),
          new FileSystemDocRepository[IO](new File(cmdLine.repositoryDirPath))
        )
        exitCode <- crawler.startCrawl(cmdLine.crawlerStartUrl, cmdLine.crawlerDepth)
      } yield exitCode
    }


  }

}