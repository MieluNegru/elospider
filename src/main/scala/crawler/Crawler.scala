package crawler

import cats.Parallel
import cats.effect.std.Semaphore
import cats.effect.{Async, Concurrent, ExitCode, Temporal}
import cats.syntax.all._
import crawler.algebra.browser.{DocBrowser, RateLimit}
import crawler.algebra.repository.DocRepository
import crawler.domain._
import org.typelevel.log4cats.slf4j.Slf4jLogger

import scala.concurrent.duration._

class Crawler[F[_] : Temporal : Parallel : Async, Url, Doc](
    browser: DocBrowser[F, Url, Doc],
    repository: DocRepository[F, Url, Doc],
) {

  private val logger = Slf4jLogger.create[F]

  // TODO crawler parallelism should be configured based on robots.txt
  val rateLimitSemaphore: F[Semaphore[F]] = Semaphore[F](50)
  val rateLimitDuration: FiniteDuration = 1.second

  // TODO: better error handling, robots.txt
  def startCrawl(initUrl: Url, maxDepth: Int): F[ExitCode] =
    for {
      log <- logger
      res <- crawl(EnqueuedUrl(initUrl, depth = 1), maxDepth).as(ExitCode.Success)
        .handleErrorWith { t =>
          Async[F].delay {
            log.error(s"Error caught: $t")
            log.error(t.getStackTrace.mkString("\n"))
          }.as(ExitCode.Error)
        }
    } yield res

  def crawl(url: EnqueuedUrl[Url], maxDepth: Int): F[Unit] =
    shouldVisit(url)
      .ifM(visit(url, maxDepth), Concurrent[F].unit)

  def shouldVisit(entry: EnqueuedUrl[Url]): F[Boolean] =
    (repository.isSaved(entry.href), browser.shouldVisit(entry))
      .mapN {
        case (false, ShouldVisit.Yes(_)) => true
        case _ => false
      }

  def visit(url: EnqueuedUrl[Url], maxDepth: Int): F[Unit] =
    for {
      _     <- logger.flatMap(_.debug(s"Visiting url ${url.href} at depth ${url.depth}"))
      doc   <- browser.loadURL(url.href)
      links <- browser.hrefs(url.href, doc)
      s     <- rateLimitSemaphore
      _     <- browser.shouldIndex(doc)
                 .ifM(repository.save(url.href, doc), Concurrent[F].unit)
      _     <- if (url.depth >= maxDepth) Concurrent[F].unit
               else links.parTraverse { link =>
                 val throttledCrawl = RateLimit(crawl(_, maxDepth), s, rateLimitDuration)
                 throttledCrawl(EnqueuedUrl(link, url.depth + 1))
               }.void
    } yield ()

}