package crawler

import java.util.concurrent.atomic.AtomicReference

import scala.concurrent.duration.Duration
import cats.effect.IO
import crawler.algebra.browser.DocBrowser
import crawler.algebra.repository.DocRepository
import crawler.domain._

// we're trying to mimic a website where each document contains 10 links of "+1 depth" to the same website
final case class TestUrl(paramDepth: Int, paramBreadth: Int = 0)
final case class TestDoc(paramDepth: Int, paramBreadth: Int)

//noinspection ConvertExpressionToSAM
object TestAlgebra {
  val result: AtomicReference[List[(TestUrl, TestDoc)]] = new AtomicReference(Nil)

  object DocBrowser extends DocBrowser[IO, TestUrl, TestDoc] {
    override def robots(url: TestUrl): IO[RobotsTxt] = IO(RobotsTxt(Duration.Zero, Seq.empty, Seq.empty))
    override def shouldVisit(url: EnqueuedUrl[TestUrl]): IO[ShouldVisit] = IO(ShouldVisit.Yes(Duration.Zero))
    override def loadURL(url: TestUrl): IO[TestDoc] = IO(TestDoc(url.paramDepth, url.paramBreadth))
    override def shouldIndex(doc: TestDoc): IO[Boolean] = IO(true)
    override def hrefs(docUrl: TestUrl, doc: TestDoc): IO[Seq[TestUrl]] = IO { (1 to 10).map(TestUrl(doc.paramDepth + 1, _)) }
  }

  object DocRepository extends DocRepository[IO, TestUrl, TestDoc] {
    override def save(url: TestUrl, doc: TestDoc): IO[Unit] = IO(result.getAndUpdate((url -> doc) :: _))
    override def load(url: TestUrl): IO[Option[TestDoc]] = IO(result.get.collectFirst { case (`url`, doc) => doc })
    override def isSaved(url: TestUrl): IO[Boolean] = IO(result.get.exists(_._1 == url))
  }

}
