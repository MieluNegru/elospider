package crawler

import cats.effect.IO
import cats.effect.testing.scalatest.AsyncIOSpec
import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers

class TestCrawler extends AsyncFunSuite with AsyncIOSpec with Matchers {

  val crawler = new Crawler[IO, TestUrl, TestDoc](TestAlgebra.DocBrowser, TestAlgebra.DocRepository)

  test("web crawler stops at given depth") {
    val depth = 10
    val url = TestUrl(paramDepth = 1)
    val program = crawler.startCrawl(url, maxDepth = depth)

    program.asserting { _ =>
      val expected = ((1, 0) +: (for(d <- 2 to depth; b <- 1 to 10) yield (d, b))).map { case (d, b) => TestUrl(d, b) -> TestDoc(d, b) }
      val actual = TestAlgebra.result.get.distinct
      actual should contain theSameElementsAs expected
    }
  }

  test("web crawler doesn't crawl same website twice") {
    val depth = 10
    val url = TestUrl(paramDepth = 1)
    val program = crawler.startCrawl(url, maxDepth = depth)

    program.asserting { _ =>
      assert(TestAlgebra.result.get.size == TestAlgebra.result.get.distinct.size)
    }
  }

}
